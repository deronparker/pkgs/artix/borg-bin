# Maintainer: Deron Parker <WkdWeWIyNHVaR1YyUUhCaGNtdGxjbk11WTJNSwo=> # base64 encoded twice

pkgname=borg-bin
pkgver=1.4.0
pkgrel=1
pkgdesc='Deduplicating backup program with compression and authenticated encryption'
arch=(x86_64)
url=https://www.borgbackup.org/
license=(custom:borgbackup-license)
depends=('glibc>=2.36')
provides=(borg borgbackup)
conflicts=(borg borgbackup)
source=(
	"borgbin-$pkgver::https://github.com/borgbackup/borg/releases/download/$pkgver/borg-linux-glibc236"
	"borgbin-$pkgver.asc::https://github.com/borgbackup/borg/releases/download/$pkgver/borg-linux-glibc236.asc"
)
validpgpkeys=('6D5BEF9ADD2075805747B70F9F88FB52FAF7B393')
sha256sums=(
	'38f148d54e8db2855a7b0f1d7f744fc25c8da82b573e2d550b2deac813a66207'
	'343001ec29418c5c36a111e67535b292a527a630f09ad68014c5acdeff3e11d1'
)

prepare() {
	reclone=false
	if test ! -d "$srcdir/borg-src"; then
		reclone=true
	else
		(
			cd "$srcdir/borg-src" || exit
			if test $(git describe) != $pkgver; then
				reclone=true
			fi
		)
	fi
	if $reclone; then
		rm "$srcdir/borg-src" -rf
		git clone --branch $pkgver --depth 1 'https://github.com/borgbackup/borg.git' "$srcdir/borg-src"
	fi
}

package() {
	# bin
	install -Dm755 "borgbin-$pkgver" "$pkgdir/usr/local/bin/borg"

	cd "$srcdir/borg-src"

	# shell completions
	install -Dm644 scripts/shell_completions/bash/borg "$pkgdir/usr/share/bash-completion/completions/borg"
	install -Dm644 scripts/shell_completions/fish/borg.fish "$pkgdir/usr/share/fish/vendor_completions.d/borg.fish"
	install -Dm644 scripts/shell_completions/zsh/_borg "$pkgdir/usr/share/zsh/site-functions/_borg"

	# man
	install -Dm644 -t "$pkgdir/usr/share/man/man1/" "docs/man/"*.1

	# LICENSE
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"

}
